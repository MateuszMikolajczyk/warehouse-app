package warehouse.app.menu.options;

public enum OptionsForSortOptionsForBooks {

    BACK(0, "BACK"),
    SORT_BOOKS_BY_TITLE(1, "SORT BOOKS BY TITLE"),
    SORT_BOOKS_BY_NUMBER_OF_PAGES(2, "SORT BOOKS BY NUMBER OF PAGES"),
    SORT_BOOKS_BY_PUBLISHER(3, "SORT BOOKS BY PUBLISHER"),
    SORT_BOOKS_BY_PRICE(4, "SORT BOOKS BY PRICE"),
    SORT_BOOKS_BY_AUTHOR(5, "SORT BOOKS BY AUTHOR"),
    SORT_BOOKS_BY_ISBN(6, "SORT BOOKS BY ISBN"),
    SORT_BOOKS_BY_ADD_DATE(7, "SORT BOOKS BY ADD DATE"),
    SORT_BOOKS(8, "SORT BOOKS");

    private int value;
    private String description;

    OptionsForSortOptionsForBooks(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
