package warehouse.data.musicCarriers;

import warehouse.utilities.DatePattern;

import java.time.LocalDateTime;
import java.util.Comparator;

public class Cd extends MusicCarrier implements Comparable<Cd> {

    private static final long serialVersionUID = 1L;

    public Cd(String performer, String title, String publisher, int price) {
        super(performer, title, publisher, price);
        setDateAndTime(LocalDateTime.now());
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append(super.toString());
        print.append(" | Time and Date of add: ");
        print.append(getDateAndTime().format(DatePattern.dateAndTimePattern()));
        print.append(" |\n");
        return print.toString();
    }

    @Override
    public int compareTo(Cd cd) {
        int numberCompare;
        numberCompare = getPerformer().compareTo(cd.getPerformer());
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = getTitle().compareTo(cd.getTitle());
        if (numberCompare != 0) {
            return numberCompare;
        }
        if (getPrice() > cd.getPrice()) {
            return 1;
        }
        if (getPrice() < cd.getPrice()) {
            return -1;
        }
        numberCompare = getPublisher().compareTo(cd.getPublisher());
        if (numberCompare != 0) {
            return numberCompare;
        }
        return getDateAndTime().compareTo(cd.getDateAndTime());
    }

    public static class CdTitleComparator implements Comparator<Cd> {
        @Override
        public int compare(Cd cd1, Cd cd2) {
            return cd1.getTitle().compareTo(cd2.getTitle());
        }
    }

    public static class CdPriceComparator implements Comparator<Cd> {
        @Override
        public int compare(Cd cd1, Cd cd2) {
            if (cd1.getPrice() > cd2.getPrice()) {
                return 1;
            }
            if (cd1.getPrice() < cd2.getPrice()) {
                return -1;
            }
            return cd1.getPrice();
        }
    }

    public static class CdPerformerComparator implements Comparator<Cd> {
        @Override
        public int compare(Cd cd1, Cd cd2) {
            return cd1.getPerformer().compareTo(cd2.getPerformer());
        }
    }

    public static class CdPublisherComparator implements Comparator<Cd> {
        @Override
        public int compare(Cd cd1, Cd cd2) {
            return cd1.getPublisher().compareTo(cd2.getPublisher());
        }
    }

    public static class CdDateAndTimeComparator implements Comparator<Cd> {
        @Override
        public int compare(Cd cd1, Cd cd2) {
            return cd1.getDateAndTime().compareTo(cd2.getDateAndTime());
        }
    }

}