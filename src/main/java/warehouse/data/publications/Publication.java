package warehouse.data.publications;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

abstract class Publication implements Serializable {

    private static final long serialVersionUID = 1L;
    private String title;
    private int pages;
    private String publisher;
    private int price;
    private LocalDateTime dateAndTime;

    public Publication(String title, int pages, String publisher, int price) {
        this.title = title;
        this.pages = pages;
        this.publisher = publisher;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(LocalDateTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Publication)) return false;
        Publication that = (Publication) o;
        return getPages() == that.getPages() &&
                getPrice() == that.getPrice() &&
                Objects.equals(getTitle(), that.getTitle()) &&
                Objects.equals(getPublisher(), that.getPublisher()) &&
                Objects.equals(getDateAndTime(), that.getDateAndTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getPages(), getPublisher(), getPrice(), getDateAndTime());
    }

    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append("| Title: ");
        print.append(getTitle());
        print.append(" | Price: ");
        print.append(getPrice());
        print.append(" | Pages: ");
        print.append(getPages());
        print.append(" | Publisher: ");
        print.append(getPublisher());
        return print.toString();
    }

}
