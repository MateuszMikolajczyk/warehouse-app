package warehouse.controlers;

import warehouse.data.musicCarriers.Cassette;
import warehouse.data.musicCarriers.Cd;
import warehouse.data.musicCarriers.Vinyl;
import warehouse.data.publications.Book;
import warehouse.data.publications.Magazine;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for managing and do some operations on values from maps with data.
 */

public class DataManagementOperations {

    private static List<Book> valuesFromBookMap;
    private static List<Magazine> valuesFromMagazineMap;
    private static List<Cd> valuesFromCdMap;
    private static List<Vinyl> valuesFromVinylMap;
    private static List<Cassette> valuesFromCassetteMap;

    /**
     * In this constructor are inicializing lists which contains values from map.
     * @param warehouseController Object of class WarehouseController.
     */
    public DataManagementOperations(WarehouseController warehouseController) {
        valuesFromBookMap = new ArrayList<>(warehouseController.getBooks().values());
        valuesFromMagazineMap = new ArrayList<>(warehouseController.getMagazines().values());
        valuesFromCdMap = new ArrayList<>(warehouseController.getCds().values());
        valuesFromVinylMap = new ArrayList<>(warehouseController.getVinyls().values());
        valuesFromCassetteMap = new ArrayList<>(warehouseController.getCassettes().values());
    }

    public static List<Book> getValuesFromBookMap() {
        return valuesFromBookMap;
    }

    public static List<Magazine> getValuesFromMagazineMap() {
        return valuesFromMagazineMap;
    }

    public static List<Cd> getValuesFromCdMap() {
        return valuesFromCdMap;
    }

    public static List<Vinyl> getValuesFromVinylMap() {
        return valuesFromVinylMap;
    }

    public static List<Cassette> getValuesFromCassetteMap() {
        return valuesFromCassetteMap;
    }

    /**
     * Method for sort and print all values from list, which is passing as parameter of method.
     * Method doesn't use comparator and values are sorted with natural order.
     * @param list Object of list with values from map.
     * @param <K> Generic type of data.
     */
    public static <K> void sortWithoutComparatorAndPrintValuesFormMap(List<K> list) {
        List<K> sorted = list.stream()
                .sorted()
                .collect(Collectors.toCollection(ArrayList::new));
        sorted.forEach(System.out::println);
    }

    /**
     * Method for sort and print all values from list, which is passing as parameter of method.
     * Method use comparator for sorting.
     * @param list Object of list with values from map.
     * @param comparator Comparator definited by user.
     * @param <K> Generic type of data.
     */
    public static <K> void sortWithComparatorAndPrintValuesFormMap(List<K> list, Comparator<K> comparator) {
        List<K> sorted = list.stream()
                .sorted(comparator)
                .collect(Collectors.toCollection(ArrayList::new));
        sorted.forEach(System.out::println);
    }

}
