package warehouse.app.menu.options;

public enum OptionsForSortOptionsForCassettes {

    BACK(0, "BACK"),
    SORT_CASSETTES_BY_TITLE(1, "SORT CASSETTES BY TITLE"),
    SORT_CASSETTES_BY_PERFORMER(2, "SORT CASSETTES BY PERFORMER"),
    SORT_CASSETTES_BY_PUBLISHER(3, "SORT CASSETTES BY PUBLISHER"),
    SORT_CASSETTES_BY_PRICE(4, "SORT CASSETTES BY PRICE"),
    SORT_CASSETTES_BY_ADD_DATE(5, "SORT CASSETTES BY ADD DATE"),
    SORT_CASSETTES(6, "SORT CASSETTES");

    private int value;
    private String description;

    OptionsForSortOptionsForCassettes(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
