package warehouse.app.menu.options;

public enum OptionsForSortOptionsForPublications {

    BACK(0, "BACK"),
    SORT_PUBLICATIONS_BY_TITLE(1, "SORT PUBLICATIONS BY TITLE"),
    SORT_PUBLICATIONS_BY_NUMBER_OF_PAGES(2, "SORT PUBLICATIONS BY NUMBER OF PAGES"),
    SORT_PUBLICATIONS_BY_PUBLISHER(3, "SORT PUBLICATIONS BY PUBLISHER"),
    SORT_PUBLICATIONS_BY_PRICE(4, "SORT PUBLICATIONS BY PRICE"),
    SORT_PUBLICATIONS_BY_ADD_DATE(5, "SORT PUBLICATIONS BY ADD DATE"),
    SORT_PUBLICATIONS(6, "SORT PUBLICATIONS");

    private int value;
    private String description;

    OptionsForSortOptionsForPublications(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
