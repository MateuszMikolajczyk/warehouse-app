package warehouse.data.publications;

import warehouse.utilities.DatePattern;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Objects;

public class Book extends Publication implements Comparable<Book> {

    private static final long serialVersionUID = 1L;
    private String author;
    private int isbn;

    public Book(String title, int pages, String publisher, String author, int isbn, int price) {
        super(title, pages, publisher, price);
        this.author = author;
        this.isbn = isbn;
        setDateAndTime(LocalDateTime.now());
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append(super.toString());
        print.append(" | Author: ");
        print.append(getAuthor());
        print.append(" | ISBN: ");
        print.append(getIsbn());
        print.append(" | Time and Date of add: ");
        print.append(getDateAndTime().format(DatePattern.dateAndTimePattern()));
        print.append(" |\n");
        return print.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return getIsbn() == book.getIsbn() &&
                Objects.equals(getAuthor(), book.getAuthor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAuthor(), getIsbn());
    }

    @Override
    public int compareTo(Book book) {
        int numberCompare;
        numberCompare = getTitle().compareTo(book.getTitle());
        if (numberCompare != 0) {
            return numberCompare;
        }
        if (getPrice() > book.getPrice()) {
            return 1;
        }
        if (getPrice() < book.getPrice()) {
            return -1;
        }
        if (getPages() > book.getPages()) {
            return 1;
        }
        if (getPages() < book.getPages()) {
            return -1;
        }
        numberCompare = getPublisher().compareTo(book.getPublisher());
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = author.compareTo(book.author);
        if (numberCompare != 0) {
            return numberCompare;
        }
        if (isbn > book.isbn) {
            return 1;
        }
        if (isbn < book.isbn) {
            return -1;
        }
        return getDateAndTime().compareTo(book.getDateAndTime());

    }

    public static class BookTitleComparator implements Comparator<Book> {
        @Override
        public int compare(Book book1, Book book2) {
            return book1.getTitle().compareTo(book2.getTitle());
        }
    }

    public static class BookPriceComparator implements Comparator<Book> {
        @Override
        public int compare(Book book1, Book book2) {
            if (book1.getPrice() > book2.getPrice()) {
                return 1;
            }
            if (book1.getPrice() < book2.getPrice()) {
                return -1;
            }
            return book1.getPrice();
        }
    }

    public static class BookPagesComparator implements Comparator<Book> {
        @Override
        public int compare(Book book1, Book book2) {
            if (book1.getPages() > book2.getPages()) {
                return 1;
            }
            if (book1.getPages() < book2.getPages()) {
                return -1;
            }
            return book1.getPages();
        }
    }

    public static class BookPublisherComparator implements Comparator<Book> {
        @Override
        public int compare(Book book1, Book book2) {
            return book1.getPublisher().compareTo(book2.getPublisher());
        }
    }

    public static class BookAuthorComparator implements Comparator<Book> {
        @Override
        public int compare(Book book1, Book book2) {
            return book1.getAuthor().compareTo(book2.getAuthor());
        }
    }

    public static class BookIsbnComparator implements Comparator<Book> {
        @Override
        public int compare(Book book1, Book book2) {
            if (book1.getIsbn() > book2.getIsbn()) {
                return 1;
            }
            if (book1.getIsbn() < book2.getIsbn()) {
                return -1;
            }
            return book1.getIsbn();
        }
    }

    public static class BookDateAndTimeComparator implements Comparator<Book> {
        @Override
        public int compare(Book book1, Book book2) {
            return book1.getDateAndTime().compareTo(book2.getDateAndTime());
        }
    }
}
