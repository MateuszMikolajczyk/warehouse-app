package warehouse.app.menu.options;

public enum OptionsForSortOptionsForCds {

    BACK(0, "BACK"),
    SORT_CDS_BY_TITLE(1, "SORT CDS BY TITLE"),
    SORT_CDS_BY_PERFORMER(2, "SORT CDS BY PERFORMER"),
    SORT_CDS_BY_PUBLISHER(3, "SORT CDS BY PUBLISHER"),
    SORT_CDS_BY_PRICE(4, "SORT CDS BY PRICE"),
    SORT_CDS_BY_ADD_DATE(5, "SORT CDS BY ADD DATE"),
    SORT_CDS(6, "SORT CDS");

    private int value;
    private String description;

    OptionsForSortOptionsForCds(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
