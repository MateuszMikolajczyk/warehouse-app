package warehouse.app.menu.options;

public enum OptionsForSortOptionsForVinyls {

    BACK(0, "BACK"),
    SORT_VINYLS_BY_TITLE(1, "SORT VINYLS BY TITLE"),
    SORT_VINYLS_BY_PERFORMER(2, "SORT VINYLS BY PERFORMER"),
    SORT_VINYLS_BY_PUBLISHER(3, "SORT VINYLS BY PUBLISHER"),
    SORT_VINYLS_BY_PRICE(4, "SORT VINYLS BY PRICE"),
    SORT_VINYLS_BY_SIZE(5, "SORT VINYLS BY SIZE"),
    SORT_VINYLS_BY_TYPE(6, "SORT VINYLS BY TYPE"),
    SORT_VINYLS_BY_ADD_DATE(7, "SORT VINYLS BY ADD DATE"),
    SORT_VINYLS(8, "SORT VINYLS");

    private int value;
    private String description;

    OptionsForSortOptionsForVinyls(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
