package warehouse.data.musicCarriers;

import warehouse.utilities.DatePattern;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Objects;

public class Vinyl extends MusicCarrier implements Comparable<Vinyl> {
    private static final long serialVersionUID = 1L;
    private String size;
    private String type;

    public Vinyl(String performer, String title, String publisher, int price, String size, String type) {
        super(performer, title, publisher, price);
        this.size = size;
        this.type = type;
        setDateAndTime(LocalDateTime.now());
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append(super.toString());
        print.append(" | Size: ");
        print.append(getSize());
        print.append(" | Type: ");
        print.append(getType());
        print.append(" | Time and Date of add: ");
        print.append(getDateAndTime().format(DatePattern.dateAndTimePattern()));
        print.append(" |\n");
        return print.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vinyl)) return false;
        if (!super.equals(o)) return false;
        Vinyl vinyl = (Vinyl) o;
        return Objects.equals(getSize(), vinyl.getSize()) &&
                Objects.equals(getType(), vinyl.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSize(), getType());
    }

    @Override
    public int compareTo(Vinyl vinyl) {
        int numberCompare;
        numberCompare = getPerformer().compareTo(vinyl.getPerformer());
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = getTitle().compareTo(vinyl.getTitle());
        if (numberCompare != 0) {
            return numberCompare;
        }
        if (getPrice() > vinyl.getPrice()) {
            return 1;
        }
        if (getPrice() < vinyl.getPrice()) {
            return -1;
        }
        numberCompare = getPublisher().compareTo(vinyl.getPublisher());
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = size.compareTo(vinyl.size);
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = type.compareTo(vinyl.type);
        if (numberCompare != 0) {
            return numberCompare;
        }
        return getDateAndTime().compareTo(vinyl.getDateAndTime());
    }

    public static class VinylTitleComparator implements Comparator<Vinyl> {
        @Override
        public int compare(Vinyl vinyl1, Vinyl vinyl2) {
            return vinyl1.getTitle().compareTo(vinyl2.getTitle());
        }
    }

    public static class VinylPriceComparator implements Comparator<Vinyl> {
        @Override
        public int compare(Vinyl vinyl1, Vinyl vinyl2) {
            if (vinyl1.getPrice() > vinyl2.getPrice()) {
                return 1;
            }
            if (vinyl1.getPrice() < vinyl2.getPrice()) {
                return -1;
            }
            return vinyl1.getPrice();
        }
    }

    public static class VinylPerformerComparator implements Comparator<Vinyl> {
        @Override
        public int compare(Vinyl vinyl1, Vinyl vinyl2) {
            return vinyl1.getPerformer().compareTo(vinyl2.getPerformer());
        }
    }

    public static class VinylPublisherComparator implements Comparator<Vinyl> {
        @Override
        public int compare(Vinyl vinyl1, Vinyl vinyl2) {
            return vinyl1.getPublisher().compareTo(vinyl2.getPublisher());
        }
    }

    public static class VinylSizeComparator implements Comparator<Vinyl> {
        @Override
        public int compare(Vinyl vinyl1, Vinyl vinyl2) {
            return vinyl1.size.compareTo(vinyl2.size);
        }
    }

    public static class VinylTypeComparator implements Comparator<Vinyl> {
        @Override
        public int compare(Vinyl vinyl1, Vinyl vinyl2) {
            return vinyl1.type.compareTo(vinyl2.type);
        }
    }

    public static class VinylDateAndTimeComparator implements Comparator<Vinyl> {
        @Override
        public int compare(Vinyl vinyl1, Vinyl vinyl2) {
            return vinyl1.getDateAndTime().compareTo(vinyl2.getDateAndTime());
        }
    }
}
