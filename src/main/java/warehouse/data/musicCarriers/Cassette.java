package warehouse.data.musicCarriers;

import warehouse.utilities.DatePattern;

import java.time.LocalDateTime;
import java.util.Comparator;

public class Cassette extends MusicCarrier implements Comparable<Cassette> {
    private static final long serialVersionUID = 1L;

    public Cassette(String performer, String title, String publisher, int price) {
        super(performer, title, publisher, price);
        setDateAndTime(LocalDateTime.now());
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append(super.toString());
        print.append(" | Time and Date of add: ");
        print.append(getDateAndTime().format(DatePattern.dateAndTimePattern()));
        print.append(" |\n");
        return print.toString();
    }

    @Override
    public int compareTo(Cassette cassette) {
        int numberCompare;
        numberCompare = getPerformer().compareTo(cassette.getPerformer());
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = getTitle().compareTo(cassette.getTitle());
        if (numberCompare != 0) {
            return numberCompare;
        }
        if (getPrice() > cassette.getPrice()) {
            return 1;
        }
        if (getPrice() < cassette.getPrice()) {
            return -1;
        }
        numberCompare = getPublisher().compareTo(cassette.getPublisher());
        if (numberCompare != 0) {
            return numberCompare;
        }
        return getDateAndTime().compareTo(cassette.getDateAndTime());
    }

    public static class CassetteTitleComparator implements Comparator<Cassette> {
        @Override
        public int compare(Cassette cassette1, Cassette cassette2) {
            return cassette1.getTitle().compareTo(cassette2.getTitle());
        }
    }

    public static class CassettePriceComparator implements Comparator<Cassette> {
        @Override
        public int compare(Cassette cassette1, Cassette cassette2) {
            if (cassette1.getPrice() > cassette2.getPrice()) {
                return 1;
            }
            if (cassette1.getPrice() < cassette2.getPrice()) {
                return -1;
            }
            return cassette1.getPrice();
        }
    }

    public static class CassettePerformerComparator implements Comparator<Cassette> {
        @Override
        public int compare(Cassette cassette1, Cassette cassette2) {
            return cassette1.getPerformer().compareTo(cassette2.getPerformer());
        }
    }

    public static class CassettePublisherComparator implements Comparator<Cassette> {
        @Override
        public int compare(Cassette cassette1, Cassette cassette2) {
            return cassette1.getPublisher().compareTo(cassette2.getPublisher());
        }
    }

    public static class CassetteDateAndTimeComparator implements Comparator<Cassette> {
        @Override
        public int compare(Cassette cassette1, Cassette cassette2) {
            return cassette1.getDateAndTime().compareTo(cassette2.getDateAndTime());
        }
    }
}
