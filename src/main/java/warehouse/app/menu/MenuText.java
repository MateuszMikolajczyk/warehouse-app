package warehouse.app.menu;

import warehouse.app.menu.options.OptionsForActionsForSorting;
import warehouse.app.menu.options.OptionsForDeleteMusicCarriers;
import warehouse.app.menu.options.OptionsForDeletePublications;
import warehouse.app.menu.options.OptionsForMainMenu;
import warehouse.app.menu.options.OptionsForMusicCarriers;
import warehouse.app.menu.options.OptionsForPublications;
import warehouse.app.menu.options.OptionsForSortOptionsForBooks;
import warehouse.app.menu.options.OptionsForSortOptionsForCassettes;
import warehouse.app.menu.options.OptionsForSortOptionsForCds;
import warehouse.app.menu.options.OptionsForSortOptionsForMagazines;
import warehouse.app.menu.options.OptionsForSortOptionsForMusicCarriers;
import warehouse.app.menu.options.OptionsForSortOptionsForPublications;
import warehouse.app.menu.options.OptionsForSortOptionsForVinyls;
import warehouse.app.menu.options.OptionsForSortOptionsForWarehouse;
import warehouse.app.menu.options.OptionsForSpecialActions;

public class MenuText {

    static String showMenu() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForMainMenu o : OptionsForMainMenu.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuSpecialActions() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSpecialActions o : OptionsForSpecialActions.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuShowPublications() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForPublications o : OptionsForPublications.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuShowMusicCarriers() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForMusicCarriers o : OptionsForMusicCarriers.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String deleteSubMenuShowPublications() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForDeletePublications o : OptionsForDeletePublications.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String deleteSubMenuShowMusicCarriers() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForDeleteMusicCarriers o : OptionsForDeleteMusicCarriers.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForActionsForSorting() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForActionsForSorting o : OptionsForActionsForSorting.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForWarehouse() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForWarehouse o : OptionsForSortOptionsForWarehouse.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForPublications() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForPublications o : OptionsForSortOptionsForPublications.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForMusicCarriers() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForMusicCarriers o : OptionsForSortOptionsForMusicCarriers.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForBooks() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForBooks o : OptionsForSortOptionsForBooks.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForMagazines() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForMagazines o : OptionsForSortOptionsForMagazines.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForCds() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForCds o : OptionsForSortOptionsForCds.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForVinyls() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForVinyls o : OptionsForSortOptionsForVinyls.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }

    static String showSubMenuChoicesForSortOptionsForCassettes() {
        StringBuilder print = new StringBuilder(32);
        for (OptionsForSortOptionsForCassettes o : OptionsForSortOptionsForCassettes.values()) {
            print.append("| " + o + "\n");
        }
        return print.toString();
    }
}
