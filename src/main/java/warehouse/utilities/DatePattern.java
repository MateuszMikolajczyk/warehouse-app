package warehouse.utilities;

import java.time.format.DateTimeFormatter;

/**
 * This class contains date formats for displaying in app.
 */
public class DatePattern {

    private static DateTimeFormatter dateTimeFormatter;

    public static DateTimeFormatter dateAndTimePattern() {
        return dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    }

}
