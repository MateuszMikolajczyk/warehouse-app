package warehouse.controlers;

import warehouse.additionalFunctions.CsvTools;
import warehouse.data.musicCarriers.Cassette;
import warehouse.data.musicCarriers.Cd;
import warehouse.data.musicCarriers.Vinyl;
import warehouse.data.publications.Book;
import warehouse.data.publications.Magazine;
import warehouse.database.MyDatabase;
import warehouse.database.WarehouseJDBC;
import warehouse.utilities.CommonTextOfInformation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.Optional;

/**
 * Main controller class with data collections and methods for managing data.
 */
public class WarehouseController {

    private LinkedHashMap<String, Book> books = null;
    private LinkedHashMap<String, Magazine> magazines = null;
    private LinkedHashMap<String, Cd> cds = null;
    private LinkedHashMap<String, Vinyl> vinyls = null;
    private LinkedHashMap<String, Cassette> cassettes = null;
    private MyDatabase myDataBase = new MyDatabase();

    public WarehouseController() {
        books = new LinkedHashMap<>();
        magazines = new LinkedHashMap<>();
        cds = new LinkedHashMap<>();
        vinyls = new LinkedHashMap<>();
        cassettes = new LinkedHashMap<>();
        loadDataFromMyDatabase();
    }

    public LinkedHashMap<String, Book> getBooks() {
        return books;
    }

    public LinkedHashMap<String, Magazine> getMagazines() {
        return magazines;
    }

    public LinkedHashMap<String, Cd> getCds() {
        return cds;
    }

    public LinkedHashMap<String, Vinyl> getVinyls() {
        return vinyls;
    }

    public LinkedHashMap<String, Cassette> getCassettes() {
        return cassettes;
    }

    public boolean addBook(Book book) {
        String bookKey = book.getTitle();

        if (books.get(bookKey) != null) {
            System.out.println(CommonTextOfInformation.thisElementAlreadyExistsInWarehouse());
            return false;
        } else {
            books.put(bookKey, book);
            //Here state of data is saving to my database.
            myDataBase.saveBook(books);
            return true;
        }
    }

    public boolean addMagazine(Magazine magazine) {
        String magazineKey = magazine.getTitle();

        if (magazines.get(magazineKey) != null) {
            System.out.println(CommonTextOfInformation.thisElementAlreadyExistsInWarehouse());
            return false;
        } else {
            magazines.put(magazineKey, magazine);
            //Here state of data is saving to my database.
            myDataBase.saveMagazine(magazines);
            return true;
        }
    }

    public boolean addCd(Cd cd) {
        String cdKey = cd.getPerformer() + cd.getTitle();

        if (cds.get(cdKey) != null) {
            System.out.println(CommonTextOfInformation.thisElementAlreadyExistsInWarehouse());
            return false;
        } else {
            cds.put(cdKey, cd);
            //Here state of data is saving to my database.
            myDataBase.saveCd(cds);
            return true;
        }
    }

    public boolean addVinyl(Vinyl vinyl) {
        String vinylKey = vinyl.getPerformer() + vinyl.getTitle();

        if (vinyls.get(vinylKey) != null) {
            System.out.println(CommonTextOfInformation.thisElementAlreadyExistsInWarehouse());
            return false;
        } else {
            vinyls.put(vinylKey, vinyl);
            //Here state of data is saving to my database.
            myDataBase.saveVinyl(vinyls);
            return true;
        }
    }

    public boolean addCassette(Cassette cassette) {
        String cassetteKey = cassette.getPerformer() + cassette.getTitle();

        if (cassettes.get(cassetteKey) != null) {
            System.out.println(CommonTextOfInformation.thisElementAlreadyExistsInWarehouse());
            return false;
        } else {
            cassettes.put(cassetteKey, cassette);
            //Here state of data is saving to my database.
            myDataBase.saveCassettes(cassettes);
            return true;
        }
    }

    public Optional<Book> getBookFromMap(String title) {
        if (books.containsKey(title)) {
            return Optional.of(books.get(title));
        }
        return Optional.empty();
    }

    public Optional<Magazine> getMagazineFromMap(String title) {
        if (magazines.containsKey(title)) {
            return Optional.of(magazines.get(title));
        }
        return Optional.empty();
    }

    public Optional<Cd> getCdFromMap(String performer, String title) {
        String performerPlusTitle = performer + title;
        if (cds.containsKey(performerPlusTitle)) {
            return Optional.of(cds.get(performerPlusTitle));
        }
        return Optional.empty();
    }

    public Optional<Vinyl> getVinylFromMap(String performer, String title) {
        String performerPlusTitle = performer + title;
        if (vinyls.containsKey(performerPlusTitle)) {
            return Optional.of(vinyls.get(performerPlusTitle));
        }
        return Optional.empty();
    }

    public Optional<Cassette> getCassetteFromMap(String performer, String title) {
        String performerPlusTitle = performer + title;
        if (cassettes.containsKey(performerPlusTitle)) {
            return Optional.of(cassettes.get(performerPlusTitle));
        }
        return Optional.empty();
    }

    public void getAllBooksAndMagazinesFromMap() {
        System.out.println("PUBLICATIONS\n" + "---------------------\n" + "BOOKS\n" + books.values() + "\nMAGAZINES\n"
                + magazines.values() + "\n");
    }

    public void getAllCdsCassettesAndVinylsFromMap() {
        System.out.println("MUSIC CARRIERS\n" + "---------------------\n" + "CDs\n" + cds.values() + "\nVINYLS\n"
                + vinyls.values() + "\nCASSETTES\n" + cassettes.values() + "\n");
    }

    /**
     * Method deletes data from maps and save current state to local database.
     */
    public void deleteAllElementsFromMap() {
        books.clear();
        magazines.clear();
        cds.clear();
        vinyls.clear();
        cassettes.clear();
        myDataBase.saveBook(books);
        myDataBase.saveMagazine(magazines);
        myDataBase.saveCd(cds);
        myDataBase.saveVinyl(vinyls);
        myDataBase.saveCassettes(cassettes);
    }

    public void deleteBookFomMap(String title) {
        books.remove(title);
        //Here state of data is saving to my database.
        myDataBase.saveBook(books);
    }

    public void deleteMagazineFromMap(String title) {
        magazines.remove(title);
        //Here state of data is saving to my database.
        myDataBase.saveMagazine(magazines);
    }

    public void deleteCdFromMap(String performer, String title) {
        String performerPlusTitle = performer + title;
        cds.remove(performerPlusTitle);
        //Here state of data is saving to my database.
        myDataBase.saveCd(cds);
    }

    public void deleteVinylFromMap(String performer, String title) {
        String performerPlusTitle = performer + title;
        vinyls.remove(performerPlusTitle);
        //Here state of data is saving to my database.
        myDataBase.saveVinyl(vinyls);
    }

    public void deleteCassetteFromMap(String performer, String title) {
        String performerPlusTitle = performer + title;
        cassettes.remove(performerPlusTitle);
        //Here state of data is saving to my database.
        myDataBase.saveCassettes(cassettes);
    }

    /**
     * Method for put data to CSV files.
     */
    public void logicForSaveWarehouseToCsv() {
        CsvTools csv = new CsvTools();
        csv.saveBooks(books);
        csv.saveMagazines(magazines);
        csv.saveCds(cds);
        csv.saveCassettes(cassettes);
        csv.saveVinyls(vinyls);
    }

    /**
     * Method for create and insert data to JDBC database.
     */
    public void logicForInsertDataToJDBCDataBase() {
        Connection connection = WarehouseJDBC.connect();
        WarehouseJDBC.createTables(connection);
        WarehouseJDBC.insertData(connection, books, magazines, cds, cassettes, vinyls);
        WarehouseJDBC.closeConnection(connection);
    }

    /**
     * Method for load data from JDBC database.
     */
    public void logicForLoadDataFromJDBCDataBase() {
        Connection connection = WarehouseJDBC.connect();
        WarehouseJDBC.loadDataFromDataBase(connection, books, magazines, cds, cassettes, vinyls);
        WarehouseJDBC.closeConnection(connection);
    }

    /**
     * Method for load data from local database.
     */
    private void loadDataFromMyDatabase() {
        Path pathBooks = Paths.get(MyDatabase.MY_DATABASE_BOOKS_FILE_NAME);
        Path pathMagazines = Paths.get(MyDatabase.MY_DATABASE_MAGAZINES_FILE_NAME);
        Path pathCd = Paths.get(MyDatabase.MY_DATABASE_CDS_FILE_NAME);
        Path pathVinyls = Paths.get(MyDatabase.MY_DATABASE_VINYLS_FILE_NAME);
        Path pathCassettes = Paths.get(MyDatabase.MY_DATABASE_CASSETTES_FILE_NAME);
        if (Files.exists(pathBooks)) {
            try (ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(MyDatabase.MY_DATABASE_BOOKS_FILE_NAME))) {

                books = (LinkedHashMap<String, Book>) ois.readObject();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (Files.exists(pathMagazines)) {
            try (ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(MyDatabase.MY_DATABASE_MAGAZINES_FILE_NAME))) {

                magazines = (LinkedHashMap<String, Magazine>) ois.readObject();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (Files.exists(pathCd)) {
            try (ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(MyDatabase.MY_DATABASE_CDS_FILE_NAME))) {

                cds = (LinkedHashMap<String, Cd>) ois.readObject();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (Files.exists(pathVinyls)) {
            try (ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(MyDatabase.MY_DATABASE_VINYLS_FILE_NAME))) {

                vinyls = (LinkedHashMap<String, Vinyl>) ois.readObject();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (Files.exists(pathCassettes)) {
            try (ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(MyDatabase.MY_DATABASE_CASSETTES_FILE_NAME))) {

                cassettes = (LinkedHashMap<String, Cassette>) ois.readObject();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

}
