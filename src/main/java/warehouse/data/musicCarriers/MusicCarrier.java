package warehouse.data.musicCarriers;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

abstract class MusicCarrier implements Serializable {

    private static final long serialVersionUID = 1L;
    private String performer;
    private String title;
    private String publisher;
    private int price;
    private LocalDateTime dateAndTime;

    public MusicCarrier(String performer, String title, String publisher, int price) {
        this.performer = performer;
        this.title = title;
        this.publisher = publisher;
        this.price = price;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(LocalDateTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append("| Performer: ");
        print.append(getPerformer());
        print.append(" | Title: ");
        print.append(getTitle());
        print.append(" | Price: ");
        print.append(getPrice());
        print.append(" | Publisher: ");
        print.append(getPublisher());
        return print.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MusicCarrier)) return false;
        MusicCarrier that = (MusicCarrier) o;
        return getPrice() == that.getPrice() &&
                Objects.equals(getPerformer(), that.getPerformer()) &&
                Objects.equals(getTitle(), that.getTitle()) &&
                Objects.equals(getPublisher(), that.getPublisher()) &&
                Objects.equals(getDateAndTime(), that.getDateAndTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPerformer(), getTitle(), getPublisher(), getPrice(), getDateAndTime());
    }
}
