package warehouse.app.menu.options;

public enum OptionsForActionsForSorting {

    BACK(0, "BACK"),
    SORT_OPTIONS_FOR_WAREHOUSE(1, "SORT OPTIONS FOR WAREHOUSE"),
    SORT_OPTIONS_FOR_PUBLICATIONS(2, "SORT OPTIONS FOR PUBLICATIONS"),
    SORT_OPTIONS_FOR_MUSIC_CARRIERS(3, "SORT OPTIONS FOR MUSIC CARRIERS"),
    SORT_OPTIONS_FOR_BOOKS(4, "SORT OPTIONS FOR BOOKS"),
    SORT_OPTIONS_FOR_MAGAZINES(5, "SORT OPTIONS FOR MAGAZINES"),
    SORT_OPTIONS_FOR_CDS(6, "SORT OPTIONS FOR CDS"),
    SORT_OPTIONS_FOR_VINYLS(7, "SORT OPTIONS FOR VINYLS"),
    SORT_OPTIONS_FOR_CASSETTES(8, "SORT OPTIONS FOR CASSETTES");

    private int value;
    private String description;

    OptionsForActionsForSorting(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
