package warehouse.app.menu.options;

public enum OptionsForSortOptionsForMagazines {

    BACK(0, "BACK"),
    SORT_MAGAZINES_BY_TITLE(1, "SORT MAGAZINES BY TITLE"),
    SORT_MAGAZINES_BY_NUMBER_OF_PAGES(2, "SORT MAGAZINES BY NUMBER OF PAGES"),
    SORT_MAGAZINES_BY_PUBLISHER(3, "SORT MAGAZINES BY PUBLISHER"),
    SORT_MAGAZINES_BY_PRICE(4, "SORT MAGAZINES BY PRICE"),
    SORT_MAGAZINES_BY_EDITION(5, "SORT MAGAZINES BY EDITION"),
    SORT_MAGAZINES_BY_ISSN(6, "SORT MAGAZINES BY ISSN"),
    SORT_MAGAZINES_BY_YEAR(7, "SORT MAGAZINES BY YEAR"),
    SORT_MAGAZINES_BY_VOLUME(8, "SORT MAGAZINES BY VOLUME"),
    SORT_MAGAZINES_BY_ADD_DATE(9, "SORT MAGAZINES BY ADD DATE"),
    SORT_MAGAZINES(10, "SORT MAGAZINES");

    private int value;
    private String description;

    OptionsForSortOptionsForMagazines(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
