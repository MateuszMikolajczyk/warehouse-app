# MUSIC STORE WAREHOUSE 
-------------------

## Description
-------------------
This is a simple application for management products in your music store. 
For now, you can store products in this app like:
> - Book
> - Magazine
> - Vinyl 
> - CD
> - Cassette

App has few features like:
> - Adding products
> - Showing products
> - Deleting products
> - Creation of CSV file with products
> - Possibility to put products to database and load from this one
> - Sorting actions for products

## Installation
-------------------
Installation is very simple, every thing which you need to do is download this code and type in cmd command.
> mvn clean install

## Run of application
-------------------
After installation, type in cmd command.
> mvn exec:java -Dexec.mainClass=warehouse.app.MainRunner

## Usage information
-------------------
You have to use numbers to select options in menu.

>Example of menu:  
>> - 0-EXIT
>> - 1-ADD A BOOK
>> - 2-ADD A MAGAZINE

# Enjoy yourself!!!!!!!