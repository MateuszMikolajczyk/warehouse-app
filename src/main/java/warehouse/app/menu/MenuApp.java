package warehouse.app.menu;

import warehouse.app.menu.options.OptionsForMainMenu;
import warehouse.controlers.LogicForMenuChoices;

import java.util.Scanner;

/**
 * Class of main menu logic.
 */
public class MenuApp {

    private static OptionsForMainMenu optionsForMainMenu = null;
    private static boolean programIsRunning = false;
    private static Scanner sc = null;

    public static void controlApp() {
        sc = new Scanner(System.in);
        programIsRunning = true;
        while (programIsRunning) {

            optionsForMainMenu = ControlClassForSubMenu.showMainMenuLogic(optionsForMainMenu, sc);

            switch (optionsForMainMenu) {
                case ADD_BOOK: {
                    LogicForMenuChoices.addBookLogic(sc);
                    break;
                }
                case ADD_MAGAZINE: {
                    LogicForMenuChoices.addMagazineLogic(sc);
                    break;
                }
                case ADD_CD: {
                    LogicForMenuChoices.addCd(sc);
                    break;
                }
                case ADD_CASSETTE: {
                    LogicForMenuChoices.addCassette(sc);
                    break;
                }
                case ADD_VINYL: {
                    LogicForMenuChoices.addVinyl(sc);
                    break;
                }
                case SHOW_PUBLICATION: {
                    ControlClassForSubMenu.showPublicationMenuLogic(programIsRunning, sc);
                    break;
                }
                case SHOW_MUSIC_CARRIER: {
                    ControlClassForSubMenu.showMusicCarriersMenuLogic(programIsRunning, sc);
                    break;
                }
                case SHOW_ALL_PUBLICATIONS: {
                    LogicForMenuChoices.getAllPublications();
                    break;
                }
                case SHOW_ALL_MUSIC_CARRIERS: {
                    LogicForMenuChoices.getAllMusicCarriers();
                    break;
                }
                case SHOW_ALL_WAREHOUSE: {
                    LogicForMenuChoices.getAllWarehouse();
                    break;
                }
                case DELETE_ALL: {
                    LogicForMenuChoices.deleteAllProducts();
                    break;
                }
                case DELETE_PUBLICATION: {
                    ControlClassForSubMenu.deletePublicationMenuLogic(programIsRunning, sc);
                    break;
                }
                case DELETE_MUSIC_CARRIER: {
                    ControlClassForSubMenu.deleteMusicCarriersMenuLogic(programIsRunning, sc);
                    break;
                }
                case SPECIAL_ACTIONS: {
                    ControlClassForSubMenu.specialActionMenuLogic(programIsRunning, sc);
                    break;
                }
                case ACTIONS_FOR_SORTING: {
                    ControlClassForSubMenu.actionsForSortingMenuLogic(programIsRunning, sc);
                    break;
                }
                case EXIT:
                    programIsRunning = false;
                    sc.close();
                    break;
            }
        }
    }
}
