package warehouse.controlers;

import warehouse.data.musicCarriers.Cassette;
import warehouse.data.musicCarriers.Cd;
import warehouse.data.musicCarriers.Vinyl;
import warehouse.data.publications.Book;
import warehouse.data.publications.Magazine;
import warehouse.utilities.CommonTextOfInformation;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class with static methods for choices in user menu.
 */
public class LogicForMenuChoices {

    private static String title = null;
    private static String performer = null;
    private static int pages = 0;
    private static String publisher = null;
    private static int price = 0;
    private static String edition = null;
    private static String issn = null;
    private static int year = 0;
    private static int volume = 0;
    private static String author = null;
    private static int isbn = 0;
    private static String size = null;
    private static String type = null;
    private static Book book = null;
    private static Magazine magazine = null;
    private static Cd cd = null;
    private static Vinyl vinyl = null;
    private static Cassette cassette = null;
    private static boolean error = false;
    private static WarehouseController warehouse = new WarehouseController();

    public static WarehouseController getWarehouse() {
        return warehouse;
    }

    public static void addBookLogic(Scanner sc) {
        error = true;
        while (error) {
            try {
                commonLogicForMusicCarriersAndPublications(sc);
                numberOfPagesForPublications(sc);
                System.out.println("Write name of author");
                author = sc.nextLine();
                System.out.println("Write ISBN");
                isbn = sc.nextInt();
                sc.nextLine();
                error = false;
                book = new Book(title, pages, publisher, author, isbn, price);
                warehouse.addBook(book);
            } catch (InputMismatchException e) {
                sc.nextLine();
                e.printStackTrace();
                System.out.println(CommonTextOfInformation.wrongCommandText());
            }
        }
    }

    public static void addMagazineLogic(Scanner sc) {
        error = true;
        while (error) {
            try {
                commonLogicForMusicCarriersAndPublications(sc);
                numberOfPagesForPublications(sc);
                System.out.println("Write edition");
                edition = sc.nextLine();
                System.out.println("Write issn");
                issn = sc.nextLine();
                System.out.println("Write year");
                year = sc.nextInt();
                sc.nextLine();
                System.out.println("Write volume");
                volume = sc.nextInt();
                sc.nextLine();
                error = false;
                magazine = new Magazine(title, pages, publisher, edition, issn, year, volume, price);
                warehouse.addMagazine(magazine);
            } catch (InputMismatchException e) {
                sc.nextLine();
                e.printStackTrace();
                System.out.println(CommonTextOfInformation.wrongCommandText());
            }
        }
    }

    public static void addCd(Scanner sc) {
        error = true;
        while (error) {
            try {
                commonLogicForMusicCarriersAndPublications(sc);
                performerForMusicCarriers(sc);
                error = false;
                cd = new Cd(performer, title, publisher, price);
                warehouse.addCd(cd);
            } catch (InputMismatchException e) {
                sc.nextLine();
                e.printStackTrace();
                System.out.println(CommonTextOfInformation.wrongCommandText());
            }
        }
    }

    public static void addCassette(Scanner sc) {
        error = true;
        while (error) {
            try {
                commonLogicForMusicCarriersAndPublications(sc);
                performerForMusicCarriers(sc);
                error = false;
                cassette = new Cassette(performer, title, publisher, price);
                warehouse.addCassette(cassette);
            } catch (InputMismatchException e) {
                sc.nextLine();
                e.printStackTrace();
                System.out.println(CommonTextOfInformation.wrongCommandText());
            }
        }
    }

    public static void addVinyl(Scanner sc) {
        error = true;
        while (error) {
            try {
                commonLogicForMusicCarriersAndPublications(sc);
                performerForMusicCarriers(sc);
                System.out.println("Write size");
                size = sc.nextLine();
                System.out.println("Write type");
                type = sc.nextLine();
                error = false;
                vinyl = new Vinyl(performer, title, publisher, price, size, type);
                warehouse.addVinyl(vinyl);
            } catch (InputMismatchException e) {
                sc.nextLine();
                e.printStackTrace();
                System.out.println(CommonTextOfInformation.wrongCommandText());
            }
        }
    }

    public static void getBook(Scanner sc) {
        warehouse.getBookFromMap(titleForPublicationsAndMusicCarriers(sc)).ifPresentOrElse(
                p -> System.out.println("BOOK: " + p),
                () -> System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText())
        );
    }

    public static void getMagazine(Scanner sc) {
        warehouse.getMagazineFromMap(titleForPublicationsAndMusicCarriers(sc)).ifPresentOrElse(
                p -> System.out.println("MAGAZINE: " + p),
                () -> System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText())
        );
    }

    public static void getCd(Scanner sc) {
        warehouse.getCdFromMap(performerForMusicCarriers(sc), titleForPublicationsAndMusicCarriers(sc)).ifPresentOrElse(
                p -> System.out.println("CD: " + p),
                () -> System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText())
        );
    }

    public static void getCassette(Scanner sc) {
        warehouse.getCassetteFromMap(performerForMusicCarriers(sc), titleForPublicationsAndMusicCarriers(sc)).ifPresentOrElse(
                p -> System.out.println("CASSETTE: " + p),
                () -> System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText())
        );
    }

    public static void getVinyl(Scanner sc) {
        warehouse.getVinylFromMap(performerForMusicCarriers(sc), titleForPublicationsAndMusicCarriers(sc)).ifPresentOrElse(
                p -> System.out.println("VINYL: " + p),
                () -> System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText())
        );
    }

    public static void getAllPublications() {
        warehouse.getAllBooksAndMagazinesFromMap();
    }

    public static void getAllMusicCarriers() {
        warehouse.getAllCdsCassettesAndVinylsFromMap();
    }

    public static void getAllWarehouse() {
        getAllPublications();
        getAllMusicCarriers();
    }

    public static void deleteAllProducts() {
        warehouse.deleteAllElementsFromMap();
        System.out.println(CommonTextOfInformation.operationEndedWithSuccessText());
    }

    public static void deleteBook(Scanner sc) {
        title = titleForPublicationsAndMusicCarriers(sc);
        if (warehouse.getBooks().get(title) != null) {
            warehouse.deleteBookFomMap(title);
            System.out.println(CommonTextOfInformation.operationEndedWithSuccessText());
        } else {
            System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText());
        }
    }

    public static void deleteMagazine(Scanner sc) {
        title = titleForPublicationsAndMusicCarriers(sc);
        if (warehouse.getMagazines().get(title) != null) {
            warehouse.deleteMagazineFromMap(title);
            System.out.println(CommonTextOfInformation.operationEndedWithSuccessText());
        } else {
            System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText());
        }
    }

    public static void deleteCd(Scanner sc) {
        title = titleForPublicationsAndMusicCarriers(sc);
        performer = performerForMusicCarriers(sc);
        if (warehouse.getCds().get(performer + title) != null) {
            warehouse.deleteCdFromMap(performer, title);
            System.out.println(CommonTextOfInformation.operationEndedWithSuccessText());
        } else {
            System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText());
        }
    }

    public static void deleteCassette(Scanner sc) {
        title = titleForPublicationsAndMusicCarriers(sc);
        performer = performerForMusicCarriers(sc);
        if (warehouse.getCassettes().get(performer + title) != null) {
            warehouse.deleteCassetteFromMap(performer, title);
            System.out.println(CommonTextOfInformation.operationEndedWithSuccessText());
        } else {
            System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText());
        }
    }

    public static void deleteVinyl(Scanner sc) {
        title = titleForPublicationsAndMusicCarriers(sc);
        performer = performerForMusicCarriers(sc);
        if (warehouse.getVinyls().get(performer + title) != null) {
            warehouse.deleteVinylFromMap(performer, title);
            System.out.println(CommonTextOfInformation.operationEndedWithSuccessText());
        } else {
            System.out.println(CommonTextOfInformation.lackOfElementInWarehouseText());
        }
    }

    public static void saveWarehouseToCsv() {
        warehouse.logicForSaveWarehouseToCsv();
    }

    public static void loadDataFromJDBCDataBase() {
        warehouse.logicForLoadDataFromJDBCDataBase();
    }

    public static void insertDataToJDBCDataBase() {
        warehouse.logicForInsertDataToJDBCDataBase();
    }

    private static void commonLogicForMusicCarriersAndPublications(Scanner sc) {
        titleForPublicationsAndMusicCarriers(sc);
        System.out.println("Write publisher");
        publisher = sc.nextLine();
        System.out.println("Write price");
        price = sc.nextInt();
        sc.nextLine();
    }

    private static void numberOfPagesForPublications(Scanner sc) {
        System.out.println("Write number of pages");
        pages = sc.nextInt();
        sc.nextLine();
    }

    private static String titleForPublicationsAndMusicCarriers(Scanner sc) {
        System.out.println("Write title");
        title = sc.nextLine();

        return title;
    }

    private static String performerForMusicCarriers(Scanner sc) {
        System.out.println("Write performer");
        performer = sc.nextLine();

        return performer;
    }

}
