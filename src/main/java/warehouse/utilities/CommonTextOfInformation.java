package warehouse.utilities;

/**
 * This class contains common text information which are using in this app..
 */
public class CommonTextOfInformation {

    public static String wrongCommandText() {
        return "Wrong command, please try again !!!";
    }

    public static String operationEndedWithSuccessText() {
        return "Operation ended with success !!!";
    }

    public static String lackOfElementInWarehouseText() {
        return "Lack of element in warehouse !!!";
    }

    public static String thisElementAlreadyExistsInWarehouse() {
        return "This element already exists in warehouse";
    }
}
