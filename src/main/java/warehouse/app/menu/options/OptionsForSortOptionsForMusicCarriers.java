package warehouse.app.menu.options;

public enum OptionsForSortOptionsForMusicCarriers {

    BACK(0, "BACK"),
    SORT_MUSIC_CARRIERS_BY_TITLE(1, "SORT MUSIC CARRIERS BY TITLE"),
    SORT_MUSIC_CARRIERS_BY_PERFORMER(2, "SORT MUSIC CARRIERS BY PERFORMER"),
    SORT_MUSIC_CARRIERS_BY_PUBLISHER(3, "SORT MUSIC CARRIERS BY PUBLISHER"),
    SORT_MUSIC_CARRIERS_BY_PRICE(4, "SORT MUSIC CARRIERS BY PRICE"),
    SORT_MUSIC_CARRIERS_BY_ADD_DATE(5, "SORT MUSIC CARRIERS BY ADD DATE"),
    SORT_MUSIC_CARRIERS(6, "SORT MUSIC CARRIERS");

    private int value;
    private String description;

    OptionsForSortOptionsForMusicCarriers(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
