package warehouse.data.publications;

import warehouse.utilities.DatePattern;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Objects;

public class Magazine extends Publication implements Comparable<Magazine> {
    private static final long serialVersionUID = 1L;
    private String edition;
    private String issn;
    private int year;
    private int volume;

    public Magazine(String title, int pages, String publisher, String edition, String issn, int year, int volume,
                    int price) {
        super(title, pages, publisher, price);
        this.edition = edition;
        this.issn = issn;
        this.year = year;
        this.volume = volume;
        setDateAndTime(LocalDateTime.now());
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append(super.toString());
        print.append(" | Edition: ");
        print.append(getEdition());
        print.append(" | ISSN: ");
        print.append(getIssn());
        print.append(" | Year: ");
        print.append(getYear());
        print.append(" | Volume: ");
        print.append(getVolume());
        print.append(" | Time and Date of add: ");
        print.append(getDateAndTime().format(DatePattern.dateAndTimePattern()));
        print.append(" |\n");
        return print.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Magazine)) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return getYear() == magazine.getYear() &&
                getVolume() == magazine.getVolume() &&
                Objects.equals(getEdition(), magazine.getEdition()) &&
                Objects.equals(getIssn(), magazine.getIssn());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getEdition(), getIssn(), getYear(), getVolume());
    }

    @Override
    public int compareTo(Magazine magazine) {
        int numberCompare;
        numberCompare = getTitle().compareTo(magazine.getTitle());
        if (numberCompare != 0) {
            return numberCompare;
        }
        if (getPrice() > magazine.getPrice()) {
            return 1;
        }
        if (getPrice() < magazine.getPrice()) {
            return -1;
        }
        if (getPages() > magazine.getPages()) {
            return 1;
        }
        if (getPages() < magazine.getPages()) {
            return -1;
        }
        numberCompare = getPublisher().compareTo(magazine.getPublisher());
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = edition.compareTo(magazine.edition);
        if (numberCompare != 0) {
            return numberCompare;
        }
        numberCompare = issn.compareTo(magazine.issn);
        if (numberCompare != 0) {
            return numberCompare;
        }
        if (year > magazine.year) {
            return 1;
        }
        if (year < magazine.year) {
            return -1;
        }
        if (volume > magazine.volume) {
            return 1;
        }
        if (volume < magazine.volume) {
            return -1;
        }
        return getDateAndTime().compareTo(magazine.getDateAndTime());
    }

    public static class MagazineTitleComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            return magazine1.getTitle().compareTo(magazine2.getTitle());
        }
    }

    public static class MagazinePriceComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            if (magazine1.getPrice() > magazine2.getPrice()) {
                return 1;
            }
            if (magazine1.getPrice() < magazine2.getPrice()) {
                return -1;
            }
            return magazine1.getPrice();
        }
    }

    public static class MagazinePagesComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            if (magazine1.getPages() > magazine2.getPages()) {
                return 1;
            }
            if (magazine1.getPages() < magazine2.getPages()) {
                return -1;
            }
            return magazine1.getPages();
        }
    }

    public static class MagazinePublisherComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            return magazine1.getPublisher().compareTo(magazine2.getPublisher());
        }
    }

    public static class MagazineEditionComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            return magazine1.getEdition().compareTo(magazine2.getEdition());
        }
    }

    public static class MagazineIssnComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            return magazine1.getIssn().compareTo(magazine2.getIssn());
        }
    }

    public static class MagazineYearComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            if (magazine1.getYear() > magazine2.getYear()) {
                return 1;
            }
            if (magazine1.getYear() < magazine2.getYear()) {
                return -1;
            }
            return magazine1.getYear();
        }
    }

    public static class MagazineVolumeComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            if (magazine1.getVolume() > magazine2.getVolume()) {
                return 1;
            }
            if (magazine1.getVolume() < magazine2.getVolume()) {
                return -1;
            }
            return magazine1.getVolume();
        }
    }

    public static class MagazineDateAndTimeComparator implements Comparator<Magazine> {
        @Override
        public int compare(Magazine magazine1, Magazine magazine2) {
            return magazine1.getDateAndTime().compareTo(magazine2.getDateAndTime());
        }
    }
}
