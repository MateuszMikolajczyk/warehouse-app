package warehouse.app.menu;

import warehouse.app.menu.options.OptionsForSortOptionsForBooks;
import warehouse.app.menu.options.OptionsForSortOptionsForCassettes;
import warehouse.app.menu.options.OptionsForSortOptionsForCds;
import warehouse.app.menu.options.OptionsForSortOptionsForMagazines;
import warehouse.app.menu.options.OptionsForSortOptionsForMusicCarriers;
import warehouse.app.menu.options.OptionsForSortOptionsForPublications;
import warehouse.app.menu.options.OptionsForSortOptionsForVinyls;
import warehouse.app.menu.options.OptionsForSortOptionsForWarehouse;
import warehouse.controlers.DataManagementOperations;
import warehouse.data.musicCarriers.Cassette;
import warehouse.data.musicCarriers.Cd;
import warehouse.data.musicCarriers.Vinyl;
import warehouse.data.publications.Book;
import warehouse.data.publications.Magazine;
import warehouse.utilities.CommonTextOfInformation;

import java.util.Scanner;

/**
 * Class contains logic of submenus for sorting all type of data.
 */
public class ControlClassForSortMenu {

    private static OptionsForSortOptionsForWarehouse optionsForSortOptionsForWarehouse = null;
    private static OptionsForSortOptionsForPublications optionsForSortOptionsForPublications = null;
    private static OptionsForSortOptionsForMusicCarriers optionsForSortOptionsForMusicCarriers = null;
    private static OptionsForSortOptionsForBooks optionsForSortOptionsForBooks = null;
    private static OptionsForSortOptionsForMagazines optionsForSortOptionsForMagazines = null;
    private static OptionsForSortOptionsForCds optionsForSortOptionsForCds = null;
    private static OptionsForSortOptionsForVinyls optionsForSortOptionsForVinyls = null;
    private static OptionsForSortOptionsForCassettes optionsForSortOptionsForCassettes = null;

    public static void sortOptionsForWarehouseMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForWarehouse());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForWarehouse = OptionsForSortOptionsForWarehouse.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForWarehouse) {
                case SORT_WAREHOUSE_BY_TITLE: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookTitleComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineTitleComparator());
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdTitleComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylTitleComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassetteTitleComparator());
                    break;
                }
                case SORT_WAREHOUSE_BY_PUBLISHER: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPublisherComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePublisherComparator());
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPublisherComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPublisherComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePublisherComparator());
                    break;
                }
                case SORT_WAREHOUSE_BY_PRICE: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPriceComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePriceComparator());
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPriceComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPriceComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePriceComparator());
                    break;
                }
                case SORT_WAREHOUSE_BY_ADD_DATE: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookDateAndTimeComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineDateAndTimeComparator());
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdDateAndTimeComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylDateAndTimeComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassetteDateAndTimeComparator());
                    break;
                }
                case SORT_WAREHOUSE: {
                    headlineForBooks();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap());
                    headlineForMagazines();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap());
                    headlineForCds();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap());
                    headlineForVinyls();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap());
                    headlineForCassettes();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void sortOptionsForPublicationsMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForPublications());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForPublications = OptionsForSortOptionsForPublications.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForPublications) {
                case SORT_PUBLICATIONS_BY_TITLE: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookTitleComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineTitleComparator());
                    break;
                }
                case SORT_PUBLICATIONS_BY_NUMBER_OF_PAGES: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPagesComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePagesComparator());
                    break;
                }
                case SORT_PUBLICATIONS_BY_PUBLISHER: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPublisherComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePublisherComparator());
                    break;
                }
                case SORT_PUBLICATIONS_BY_PRICE: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPriceComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePriceComparator());
                    break;
                }
                case SORT_PUBLICATIONS_BY_ADD_DATE: {
                    headlineForBooks();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookDateAndTimeComparator());
                    headlineForMagazines();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineDateAndTimeComparator());
                    break;
                }
                case SORT_PUBLICATIONS: {
                    headlineForBooks();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap());
                    headlineForMagazines();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void sortOptionsForMusicCarriersMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForMusicCarriers());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForMusicCarriers = OptionsForSortOptionsForMusicCarriers.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForMusicCarriers) {
                case SORT_MUSIC_CARRIERS_BY_TITLE: {
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdTitleComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylTitleComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassetteTitleComparator());
                    break;
                }
                case SORT_MUSIC_CARRIERS_BY_PERFORMER: {
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPerformerComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPerformerComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePerformerComparator());
                    break;
                }
                case SORT_MUSIC_CARRIERS_BY_PUBLISHER: {
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPublisherComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPublisherComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePublisherComparator());
                    break;
                }
                case SORT_MUSIC_CARRIERS_BY_PRICE: {
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPriceComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPriceComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePriceComparator());
                    break;
                }
                case SORT_MUSIC_CARRIERS_BY_ADD_DATE: {
                    headlineForCds();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdDateAndTimeComparator());
                    headlineForVinyls();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylDateAndTimeComparator());
                    headlineForCassettes();
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassetteDateAndTimeComparator());
                    break;
                }
                case SORT_MUSIC_CARRIERS: {
                    headlineForCds();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap());
                    headlineForVinyls();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap());
                    headlineForCassettes();
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void sortOptionsForBooksMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForBooks());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForBooks = OptionsForSortOptionsForBooks.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForBooks) {
                case SORT_BOOKS_BY_TITLE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookTitleComparator());
                    break;
                }
                case SORT_BOOKS_BY_NUMBER_OF_PAGES: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPagesComparator());
                    break;
                }
                case SORT_BOOKS_BY_PUBLISHER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPublisherComparator());
                    break;
                }
                case SORT_BOOKS_BY_PRICE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookPriceComparator());
                    break;
                }
                case SORT_BOOKS_BY_AUTHOR: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookAuthorComparator());
                    break;
                }
                case SORT_BOOKS_BY_ISBN: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookIsbnComparator());
                    break;
                }
                case SORT_BOOKS_BY_ADD_DATE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap(), new Book.BookDateAndTimeComparator());
                    break;
                }
                case SORT_BOOKS: {
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromBookMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void sortOptionsForMagazinesMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForMagazines());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForMagazines = OptionsForSortOptionsForMagazines.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForMagazines) {
                case SORT_MAGAZINES_BY_TITLE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineTitleComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_NUMBER_OF_PAGES: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePagesComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_PUBLISHER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePublisherComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_PRICE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazinePriceComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_EDITION: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineEditionComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_ISSN: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineIssnComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_YEAR: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineYearComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_VOLUME: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineVolumeComparator());
                    break;
                }
                case SORT_MAGAZINES_BY_ADD_DATE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap(), new Magazine.MagazineDateAndTimeComparator());
                    break;
                }
                case SORT_MAGAZINES: {
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromMagazineMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void sortOptionsForCdsMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForCds());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForCds = OptionsForSortOptionsForCds.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForCds) {
                case SORT_CDS_BY_TITLE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdTitleComparator());
                    break;
                }
                case SORT_CDS_BY_PERFORMER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPerformerComparator());
                    break;
                }
                case SORT_CDS_BY_PUBLISHER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPublisherComparator());
                    break;
                }
                case SORT_CDS_BY_PRICE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdPriceComparator());
                    break;
                }
                case SORT_CDS_BY_ADD_DATE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap(), new Cd.CdDateAndTimeComparator());
                    break;
                }
                case SORT_CDS: {
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCdMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void sortOptionsForVinylsMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForVinyls());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForVinyls = OptionsForSortOptionsForVinyls.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForVinyls) {
                case SORT_VINYLS_BY_TITLE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylTitleComparator());
                    break;
                }
                case SORT_VINYLS_BY_PERFORMER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPerformerComparator());
                    break;
                }
                case SORT_VINYLS_BY_PUBLISHER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPublisherComparator());
                    break;
                }
                case SORT_VINYLS_BY_PRICE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylPriceComparator());
                    break;
                }
                case SORT_VINYLS_BY_SIZE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylSizeComparator());
                    break;
                }
                case SORT_VINYLS_BY_TYPE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylTypeComparator());
                    break;
                }
                case SORT_VINYLS_BY_ADD_DATE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap(), new Vinyl.VinylDateAndTimeComparator());
                    break;
                }
                case SORT_VINYLS: {
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromVinylMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void sortOptionsForCassettesMenuLogic(boolean error, boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForSortOptionsForCassettes());
                try {
                    int input = ControlClassForSubMenu.handleMenuOptionInput(scanner);
                    optionsForSortOptionsForCassettes = OptionsForSortOptionsForCassettes.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSortOptionsForCassettes) {
                case SORT_CASSETTES_BY_TITLE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassetteTitleComparator());
                    break;
                }
                case SORT_CASSETTES_BY_PERFORMER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePerformerComparator());
                    break;
                }
                case SORT_CASSETTES_BY_PUBLISHER: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePublisherComparator());
                    break;
                }
                case SORT_CASSETTES_BY_PRICE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassettePriceComparator());
                    break;
                }
                case SORT_CASSETTES_BY_ADD_DATE: {
                    DataManagementOperations.sortWithComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap(), new Cassette.CassetteDateAndTimeComparator());
                    break;
                }
                case SORT_CASSETTES: {
                    DataManagementOperations.sortWithoutComparatorAndPrintValuesFormMap(DataManagementOperations.getValuesFromCassetteMap());
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    private static void headlineForBooks() {
        System.out.println("|||BOOKS|||");
    }

    private static void headlineForMagazines() {
        System.out.println("|||MAGAZINES|||");
    }

    private static void headlineForCds() {
        System.out.println("|||CDs|||");
    }

    private static void headlineForVinyls() {
        System.out.println("|||VINYLS|||");
    }

    private static void headlineForCassettes() {
        System.out.println("|||CASSETTES|||");
    }
}
