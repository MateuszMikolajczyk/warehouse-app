package warehouse.app.menu.options;

public enum OptionsForMusicCarriers {

    BACK(0, "BACK"),
    SHOW_CD(1, "SHOW CD"),
    SHOW_VINYL(2, "SHOW VINYL"),
    SHOW_CASSETTE(3, "SHOW CASSETTE");

    private int value;
    private String description;

    OptionsForMusicCarriers(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
