package warehouse.app;

import warehouse.app.menu.MenuApp;

/**
 * <h1>MUSIC STORE WAREHOUSE</h1>
 * By this app you can manage products in your music store.
 * For now you can add products like:
 * - Book
 * - Magazine
 * - Vinyl
 * - CD
 * - Cassette
 *
 * @author Mateusz Mikołajczyk
 * @version 1.0
 * @since 2020-12-04
 */
public class MainRunner {

    /**
     * Main method for run app.
     */
    public static void main(String[] args) {
        System.out.println("[------------------------MUSIC STORE WAREHOUSE APP------------------------]");
        MenuApp.controlApp();
    }

}
