package warehouse.app.menu.options;

public enum OptionsForSortOptionsForWarehouse {

    BACK(0, "BACK"),
    SORT_WAREHOUSE_BY_TITLE(1, "SORT WAREHOUSE BY TITLE"),
    SORT_WAREHOUSE_BY_PUBLISHER(2, "SORT WAREHOUSE BY PUBLISHER"),
    SORT_WAREHOUSE_BY_PRICE(3, "SORT WAREHOUSE BY PRICE"),
    SORT_WAREHOUSE_BY_ADD_DATE(4, "SORT WAREHOUSE BY ADD DATE"),
    SORT_WAREHOUSE(5, "SORT WAREHOUSE");

    private int value;
    private String description;

    OptionsForSortOptionsForWarehouse(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return value + "-" + description;
    }
}
