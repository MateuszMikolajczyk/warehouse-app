package warehouse.app.menu;

import warehouse.app.menu.options.OptionsForActionsForSorting;
import warehouse.app.menu.options.OptionsForDeleteMusicCarriers;
import warehouse.app.menu.options.OptionsForDeletePublications;
import warehouse.app.menu.options.OptionsForMainMenu;
import warehouse.app.menu.options.OptionsForMusicCarriers;
import warehouse.app.menu.options.OptionsForPublications;
import warehouse.app.menu.options.OptionsForSpecialActions;
import warehouse.controlers.DataManagementOperations;
import warehouse.controlers.LogicForMenuChoices;
import warehouse.utilities.CommonTextOfInformation;

import java.util.Scanner;

/**
 * Class contains logic of submenus.
 */
public class ControlClassForSubMenu {

    private static OptionsForSpecialActions optionsForSpecialActions = null;
    private static OptionsForPublications optionsForPublications = null;
    private static OptionsForMusicCarriers optionsForMusicCarriers = null;
    private static OptionsForDeletePublications optionsForDeletePublications = null;
    private static OptionsForDeleteMusicCarriers optionsForDeleteMusicCarriers = null;
    private static OptionsForActionsForSorting optionsForActionsForSorting = null;
    private static DataManagementOperations dataManagementOperations = null;
    private static boolean error = false;
    private static String input = null;

    public static OptionsForMainMenu showMainMenuLogic(OptionsForMainMenu optionsForMainMenu, Scanner sc) {

        error = true;
        while (error) {
            System.out.println(MenuText.showMenu());
            try {
                int input = handleMenuOptionInput(sc);
                optionsForMainMenu = OptionsForMainMenu.values()[input];
                error = false;
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                ex.printStackTrace();
                System.err.println(CommonTextOfInformation.wrongCommandText());
            }
        }

        return optionsForMainMenu;
    }

    public static void showPublicationMenuLogic(boolean programIsRunning, Scanner sc) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuShowPublications());
                try {
                    int input = handleMenuOptionInput(sc);
                    optionsForPublications = OptionsForPublications.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForPublications) {
                case SHOW_BOOK: {
                    LogicForMenuChoices.getBook(sc);
                    break;
                }
                case SHOW_MAGAZINE: {
                    LogicForMenuChoices.getMagazine(sc);
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void actionsForSortingMenuLogic(boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            dataManagementOperations = new DataManagementOperations(LogicForMenuChoices.getWarehouse());
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuChoicesForActionsForSorting());
                try {
                    int input = handleMenuOptionInput(scanner);
                    optionsForActionsForSorting = OptionsForActionsForSorting.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForActionsForSorting) {
                case SORT_OPTIONS_FOR_WAREHOUSE: {
                    ControlClassForSortMenu.sortOptionsForWarehouseMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case SORT_OPTIONS_FOR_PUBLICATIONS: {
                    ControlClassForSortMenu.sortOptionsForPublicationsMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case SORT_OPTIONS_FOR_MUSIC_CARRIERS: {
                    ControlClassForSortMenu.sortOptionsForMusicCarriersMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case SORT_OPTIONS_FOR_BOOKS: {
                    ControlClassForSortMenu.sortOptionsForBooksMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case SORT_OPTIONS_FOR_MAGAZINES: {
                    ControlClassForSortMenu.sortOptionsForMagazinesMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case SORT_OPTIONS_FOR_CDS: {
                    ControlClassForSortMenu.sortOptionsForCdsMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case SORT_OPTIONS_FOR_VINYLS: {
                    ControlClassForSortMenu.sortOptionsForVinylsMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case SORT_OPTIONS_FOR_CASSETTES: {
                    ControlClassForSortMenu.sortOptionsForCassettesMenuLogic(error, programIsRunning, scanner);
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void specialActionMenuLogic(boolean programIsRunning, Scanner scanner) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuSpecialActions());
                try {
                    int input = handleMenuOptionInput(scanner);
                    optionsForSpecialActions = OptionsForSpecialActions.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForSpecialActions) {
                case CREATE_CSV: {
                    LogicForMenuChoices.saveWarehouseToCsv();
                    break;
                }
                case INSERT_DATA_FROM_WAREHOUSE_TO_DATABASE: {
                    LogicForMenuChoices.insertDataToJDBCDataBase();
                    break;
                }
                case LOAD_DATA_FROM_DATABASE_TO_WAREHOUSE: {
                    LogicForMenuChoices.loadDataFromJDBCDataBase();
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void showMusicCarriersMenuLogic(boolean programIsRunning, Scanner sc) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.showSubMenuShowMusicCarriers());
                try {
                    int input = handleMenuOptionInput(sc);
                    optionsForMusicCarriers = OptionsForMusicCarriers.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForMusicCarriers) {
                case SHOW_CD: {
                    LogicForMenuChoices.getCd(sc);
                    break;
                }
                case SHOW_VINYL: {
                    LogicForMenuChoices.getVinyl(sc);
                    break;
                }
                case SHOW_CASSETTE: {
                    LogicForMenuChoices.getCassette(sc);
                    break;
                }
                case BACK: {
                    return;
                }
            }

        }
    }

    public static void deletePublicationMenuLogic(boolean programIsRunning, Scanner sc) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.deleteSubMenuShowPublications());
                try {
                    int input = handleMenuOptionInput(sc);
                    optionsForDeletePublications = OptionsForDeletePublications.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }
            switch (optionsForDeletePublications) {
                case DELETE_BOOK: {
                    LogicForMenuChoices.deleteBook(sc);
                    break;
                }
                case DELETE_MAGAZINE: {
                    LogicForMenuChoices.deleteMagazine(sc);
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    public static void deleteMusicCarriersMenuLogic(boolean programIsRunning, Scanner sc) {

        while (programIsRunning) {
            error = true;
            while (error) {
                System.out.println(MenuText.deleteSubMenuShowMusicCarriers());
                try {
                    int input = handleMenuOptionInput(sc);
                    optionsForDeleteMusicCarriers = OptionsForDeleteMusicCarriers.values()[input];
                    error = false;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
                    ex.printStackTrace();
                    System.err.println(CommonTextOfInformation.wrongCommandText());
                }
            }

            switch (optionsForDeleteMusicCarriers) {
                case DELETE_CD: {
                    LogicForMenuChoices.deleteCd(sc);
                    break;
                }
                case DELETE_VINYL: {
                    LogicForMenuChoices.deleteVinyl(sc);
                    break;
                }
                case DELETE_CASSETTE: {
                    LogicForMenuChoices.deleteCassette(sc);
                    break;
                }
                case BACK: {
                    return;
                }
            }
        }
    }

    /**
     * Method for entering user choice in menu.
     * @param sc Object of class Scanner.
     * @return Method Returns String parsed to int.
     */
    public static int handleMenuOptionInput(Scanner sc) {

        System.out.println("|Enter the number of your choice|");
        input = sc.nextLine();
        return Integer.parseInt(input);
    }
}
